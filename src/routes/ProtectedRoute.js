import React from 'react'
import { Route } from 'react-router-dom'

import { ResponsiveLayer } from '../containers'


const ProtectedRoute = ({
  path,
  component
}) => {

  return (
    <ResponsiveLayer>
      <Route
        exact
        path={path}
        component={component}
      />
    </ResponsiveLayer>
  )
}

export default ProtectedRoute
