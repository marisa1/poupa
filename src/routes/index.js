import React from 'react'
import { Switch, Route, BrowserRouter as Router, Redirect } from 'react-router-dom'
import ProtectedRoute from './ProtectedRoute'

import {
  Home,
  Login,
  Settings,
  Profile
} from '../pages'

const Root = () => {
  return (
    <Router>
      <Switch>
        <Route
          exact
          path="/login"
          component={Login}
        />
        <ProtectedRoute
          path="/home"
          component={Home}
        />
        <ProtectedRoute
          path="/settings"
          component={Settings}
        />
        <ProtectedRoute
          path="/profile"
          component={Profile}
        />
        <Route exact path="/">
          <Redirect to="/login"/>
        </Route>
      </Switch>
    </Router>
  )
}

export default Root
