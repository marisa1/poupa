import React, { useEffect, useState } from 'react'
import { HomeContainer } from '../../containers'
import { getSuppliers } from '../../utils/request'

const Home = () => {
  const [suppliers, setSuppliers] = useState([])

  useEffect(() => {
    const getSuppliersList = async () => {
      const suppliersList = await getSuppliers()
      setSuppliers(suppliersList)
    }

    getSuppliersList()
  }, [setSuppliers])

  return (
    <HomeContainer suppliers={suppliers} />
  )
}

export default Home