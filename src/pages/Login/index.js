import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { LoginContainer } from '../../containers'
import { login } from '../../utils/request'
import { setUserToken } from '../../utils/storage';

const Login = () => {
  const [error, setError] = useState('')
  const history = useHistory()

  const onSubmit = async (formData) => {
    const response = await login(formData)

    if(response && response.error){
      return setError(response.message)
    }

    const {
      access_token: accessToken
    } = response

    setUserToken(accessToken)

    history.push("/home")
  }

  return (
    <LoginContainer onSubmit={onSubmit} error={error} />
  )
}

export default Login