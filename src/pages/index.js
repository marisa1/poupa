import Login from './Login'
import Home from './Home'
import Profile from './Profile'
import Settings from './Settings'

export {
  Login,
  Home,
  Settings,
  Profile
}