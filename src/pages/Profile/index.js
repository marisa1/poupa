import React, { useState, useEffect } from 'react'


import { getUser } from '../../utils/request'
import { ProfileContainer } from '../../containers'

const Profile = () => {
  const [loading, setLoading] = useState(false)
  const [user, setUser] = useState({
    roles: [{}]
  })

  useEffect(() => {
    const setUserState = async () => {
      setLoading(true)

      const response = await getUser()
      setUser(response)

      setLoading(false)
    }

    setUserState()
  }, [setUser])

  return (
    <ProfileContainer user={user} loading={loading} />
  )
}

export default Profile