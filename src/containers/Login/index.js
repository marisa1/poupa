import React, { useState } from 'react'
import { Button, Input } from '../../components'
import { ReactComponent as HomeIllustration } from '../../assets/images/home.svg'

import style from './style.module.css'

const Login = ({ error, onSubmit }) => {
  const [formValues, setFormValues] = useState({
    "grant_type": "password",
  })

  const onChange = ({name, value}) => {
    setFormValues({ ...formValues, [name]: value })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    onSubmit(formValues)
  }

  return (
    <div className={style.main}>
      <div className={style.container}>
        <div className={style.content}>
          <p className={style.contentTitle}>Poupa+</p>
          <HomeIllustration width="250px" height="250px" />
          <span>Gerencie seus fornecedores de maneira <b>rápida e fácil!</b></span>
        </div>
        <div className={style.loginBox}>
          <p className={style.loginTitle}>Boas Vindas!</p>
          <form onSubmit={handleSubmit}>
            <Input value={formValues.email} name="username" type="text" required placeholder="E-mail" handleChange={onChange} />
            <Input value={formValues.password} name="password" type="password" required placeholder="Password" handleChange={onChange} />
            {error && <span>{error}</span>}
            {/* acessa com Auth0 */}
            <Button type="submit" text="Entrar"/>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Login