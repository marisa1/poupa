import React from 'react'

import { Button, Loading } from '../../components'

import { ReactComponent as PersonIcon } from '../../assets/icons/person.svg'
import style from './style.module.css'

const Profile = ({user, loading}) => {

  return (
    <Loading isLoading={loading}>
      <div className={style.content}>
        <div className={style.profile}>
          <div className={style.image}>
            <PersonIcon width="100px" />
            <p>{user.name}</p>
            <p className={style.role}>
              {user.roles[0].name === "ROLE_ADMIN" ? "Admin" : "Usuário padrão" }
            </p>
          </div>
          <span>E-mail</span>
          <p>{user.email}</p>
          <span>Telefone</span>
          <p>{user.phoneNumber}</p>
        <Button text="Sair" />
        </div>
      </div>
    </Loading>
  )
}

export default Profile