import React from 'react'
import PropTypes from 'prop-types'
import { SupplierItem } from '../../components'
import style from './style.module.css'

const Home = ({ suppliers }) => {

  const supplierList = suppliers.map((info) => (
    <SupplierItem info={info} key={info.publicId} />
  ))

  return (
    <div>
      <div className={style.main}>
        {/* <div>
          <Input required placeholder="Buscar" />
        </div> */}
        <div className={style.content}>
          <div className={style.supplierHeader}>
            <p>Fornecedores</p>
            <span>Adicionar Fornecedor</span>
          </div>
          {supplierList}
        </div>
      </div>
    </div>
  )
}

Home.propTypes = {
  suppliers: PropTypes.arrayOf(
    PropTypes.shape({
      publicId: PropTypes.string,
      name: PropTypes.string,
      cnpj: PropTypes.string,
      phoneNumber: PropTypes.string,
      address: PropTypes.string,
      number: PropTypes.string,
      complement: PropTypes.string,
      neighborhood: PropTypes.string,
      city: PropTypes.string,
      state: PropTypes.string
    })
  ).isRequired,
  user: PropTypes.shape({
    name: PropTypes.string,
  }).isRequired,
}

export default Home