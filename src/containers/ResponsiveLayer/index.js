import React, { Fragment, useState, useEffect } from 'react'
import { getUser } from '../../utils/request'

import { Sidebar } from '../../components'
import { ReactComponent as ListIcon } from '../../assets/icons/list.svg'
import { ReactComponent as ConfigIcon } from '../../assets/icons/config.svg'

import style from './style.module.css'

const values = [
  {
    icon: ListIcon,
    label: "Inicio",
    to: "/home",
  },
  {
    icon: ConfigIcon,
    label: "Configuraçoes",
    to: "/settings",
  }
]

const ResponsiveLayer = ({
  children
}) =>  {
  const [user, setUser] = useState({})

  useEffect(() => {
    const setUserState = async () => {
      const response = await getUser()
      setUser(response)
    }

    setUserState()
  }, [setUser])

  return (
    <Fragment>
      <div className={style.sidebar}>
        <Sidebar values={values} username={user.name} />
      </div>
      <div className={style.content}>
        {children}
      </div>
    </Fragment>
  )

}

export default ResponsiveLayer
