import React, { useState } from 'react'

import { formatAddress } from '../../utils/formatters'
import style from './style.module.css'
import { ReactComponent as ArrowIcon } from '../../assets/icons/next.svg'
import { ReactComponent as PersonIcon } from '../../assets/icons/person.svg'
import { ReactComponent as DeleteIcon } from '../../assets/icons/delete.svg'
import { ReactComponent as EditIcon } from '../../assets/icons/pencil.svg'

const SupplierItem = ({
  info
}) => {
  const [collapsed, setCollapsed] = useState(true)

  console.log(info)

  const {
    // publicId,
    name,
    cnpj,
    phoneNumber,
    address,
    number,
    complement,
    neighborhood,
    city,
    state
  } = info

  const collapseClass = collapsed
    ? style.collapsedDetail
    : style.openDetail

  const arrowClass = collapsed
    ? style.defaultArrow
    : style.rotateArrow

  const toggleCollapse = () => {
    setCollapsed(!collapsed)
  }

  const formattedAddress = formatAddress({
    address,
    number,
    complement,
    neighborhood,
    city,
    state
  })

  return (
    <div>
      <div className={style.container} onClick={toggleCollapse}>
        <div className={style.detail}>
          <PersonIcon className={style.personIcon} width="40px" />
          <div className={style.description}>
            <p>{name}</p>
            <span>{cnpj}</span>
          </div>
        </div>
        <ArrowIcon className={arrowClass} width="20px" />
      </div>
      <div className={collapseClass}>
        <div className={style.detailItem}>
          <p>Telefone</p>
          <p>{phoneNumber}</p>
        </div>
        <div className={style.detailItem}>
          <p>Endereço</p>
          <p>{formattedAddress}</p>
        </div>
        <div className={style.actionButtons} >
          <DeleteIcon className={style.deleteIcon} title="Excluir" width="20px" />
          <EditIcon className={style.editIcon} width="20px" title="Editar" />
        </div>
      </div>
    </div>
  )
}

export default SupplierItem