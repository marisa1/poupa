import React from 'react'

import style from './style.module.css'

const Input = ({
  handleChange,
  name,
  placeholder,
  required,
  value,
  type
}) => {
  const onChange = ({ target: { name, value }}) => {
    handleChange({name, value})
  }

  return (
    <div className={style.container}>
      <input name={name} required={required} className={style.input} value={value} type={type} onChange={onChange} />
      <span className={style.label}>{placeholder}</span>
    </div>
  )
}

export default Input