import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { ReactComponent as ArrowIcon } from '../../assets/icons/next.svg'

import style from './style.module.css'

const Sidebar = ({
  username,
  values
}) => {
  const children = values.map(({icon: Icon, label, to}) => (
    <Link className={style.item} to={to} key={label}>
      {Icon && <Icon width="20px" />}
      <span className={style.itemLink} >{label}</span>
    </Link>
  )
)

  return (
    <div className={style.content}>
      <div className={style.titleWrapper}>
        <p className={style.title}>POUPA+</p>
        {children}
      </div>
      <Link className={style.profile} to="/profile" >
        <span>{username}</span>
        <ArrowIcon width="20px" />
      </Link>
    </div>
  )
}

Sidebar.propTypes = {
  username: PropTypes.string,
  values: PropTypes.arrayOf(
    PropTypes.shape({
      icon: PropTypes.object,
      label: PropTypes.string,
      to: PropTypes.string,
    })
  ).isRequired,
}

Sidebar.defaultProps = {
  username: '',
}

export default Sidebar
