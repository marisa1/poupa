import React from 'react'

import style from './style.module.css'

const Loading = ({
  isLoading,
  children
}) => {

  return (
    <div>
      {isLoading
        ? (
          <div className={style.content}>
            <div className={style.loader}>
            </div>
          </div>
          )
        : children
      }
    </div>
  )
}

export default Loading
