import React from 'react'

import style from './style.module.css'

const Button = ({
  handleClick,
  text,
  type
}) => (
  <div className={style.container}>
    <button onClick={handleClick} type={type} className={style.button}>{text}</button>
  </div>
)

export default Button