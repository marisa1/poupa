const USER_TOKEN = 'user_session_token'

export function getUserToken () {
  return localStorage.getItem(USER_TOKEN)
}

export function setUserToken (value) {
  return localStorage.setItem(USER_TOKEN, String(value))
}

export function removeUserToken () {
  return localStorage.removeItem(USER_TOKEN)
}
