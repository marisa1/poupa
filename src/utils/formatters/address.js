const formatAddress = ({
  address,
  number,
  complement,
  neighborhood,
  city,
  state
}) => {
  const hasComplement = complement ? complement : 'Sem Complemento'

  return `${address}, ${number} - ${hasComplement} | ${neighborhood}, ${city} - ${state}`
}

export default formatAddress