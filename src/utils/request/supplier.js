import { getUserToken } from '../storage'
const BASE_URL = "https://2gym6eoyk9.execute-api.sa-east-1.amazonaws.com/default"

const getSuppliers = async () => {
  const response = await fetch(`${BASE_URL}/suppliers`,
  {
    method: "GET",
    headers: {
      "Authorization": `Bearer ${getUserToken()}`,
    },
  })

  return await response.json()
}

const getSupplier = () => {

}

const postSupplier = () => {

}

const putSupplier = () => {

}

const delSupplier = () => {

}

export {
  getSuppliers,
  getSupplier,
  postSupplier,
  putSupplier,
  delSupplier
}