import { getUserToken } from '../storage'
const BASE_URL = "https://2gym6eoyk9.execute-api.sa-east-1.amazonaws.com/default"

const getUser = async () => {
  const getCurrentUser = await fetch(
    `${BASE_URL}/users`,
    {
      method: "GET",
      headers: {
        "Authorization": `Bearer ${getUserToken()}`,
      },
    }
  )

  const currentUser = await getCurrentUser.json()

  const getCurrentUserDetail = await fetch(
    `${BASE_URL}/users/${currentUser[0].publicId} `,
    {
      method: "GET",
      headers: {
        "Authorization": `Bearer ${getUserToken()}`,
      },
    }
  )

  return await getCurrentUserDetail.json()
}

export default getUser