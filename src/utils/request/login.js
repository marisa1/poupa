
const BASE_URL = "https://2gym6eoyk9.execute-api.sa-east-1.amazonaws.com/default"

const username = "rest-api"
const password = "aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee"

const login = async (formValues) => {
  const formData = new FormData()

  for(const name in formValues) {
    formData.append(name, formValues[name]);
  }

  try {
    const response = await fetch(
      `${BASE_URL}/oauth/token`, {
      method: "POST",
      headers: {
        "Authorization": `Basic ${window.btoa(username + ':' + password)}`,
      },
      body: formData,
      }
    )

    if(response.status === 400) {
      return {
        error: 400,
        message: "Usuário ou senha incorretos."
      }
    }

    return await response.json()
  } catch (e) {
    return e
  }
}

export default login